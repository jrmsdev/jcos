.PHONY: all
all: kernel

.PHONY: kernel
kernel:
	@$(MAKE) -C kernel all

.PHONY: clean
clean:
	@$(MAKE) -C kernel clean

.PHONY: buildtools
buildtools:
	@$(MAKE) -C build all

.PHONY: distclean
distclean:
	@$(MAKE) -C build clean
