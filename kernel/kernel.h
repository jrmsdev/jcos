/* Copyright (c) 2018, Jeremías Casteglione */
/* Copyright (c) 2018, Carlos Fenollosa */

#ifndef KERNEL_H
#define KERNEL_H

void user_input(char *input);

#endif
