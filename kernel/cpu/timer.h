/* Copyright (c) 2018, Jeremías Casteglione */
/* Copyright (c) 2018, Carlos Fenollosa */

#ifndef CPU_TIMER_H
#define CPU_TIMER_H

#include <stdint.h>

void init_timer(uint32_t freq);

#endif /* CPU_TIMER_H */
