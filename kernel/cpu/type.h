/* Copyright (c) 2018, Jeremías Casteglione */
/* Copyright (c) 2018, Carlos Fenollosa */

#ifndef CPU_TYPE_H
#define CPU_TYPE_H

#include <stdint.h>

#define low_16(address) (uint16_t)((address) & 0xFFFF)
#define high_16(address) (uint16_t)(((address) >> 16) & 0xFFFF)

#endif /* CPU_TYPE_H */
