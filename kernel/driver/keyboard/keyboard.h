/* Copyright (c) 2018, Jeremías Casteglione */
/* Copyright (c) 2018, Carlos Fenollosa */

#ifndef DRIVERS_KEYBOARD_H
#define DRIVERS_KEYBOARD_H

void init_keyboard();

#endif /* DRIVERS_KEYBOARD_H */
