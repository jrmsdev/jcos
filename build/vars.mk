PREFIX ?= /opt/jcos
TARGET ?= i386-elf

CC ?= gcc8
CXX ?= g++8

TOOLS_PREFIX ?= /usr/local/bin/

AR := $(TOOLS_PREFIX)ar
AS := $(TOOLS_PREFIX)as
LD := $(TOOLS_PREFIX)ld
NM := $(TOOLS_PREFIX)nm
RANLIB := $(TOOLS_PREFIX)ranlib
STRIP := $(TOOLS_PREFIX)strip
OBJCOPY := $(TOOLS_PREFIX)objcopy
OBJDUMP := $(TOOLS_PREFIX)objdump
READELF := $(TOOLS_PREFIX)readelf
